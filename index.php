<?php include("conexion.php"); ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registro</title>
    <link rel="stylesheet" href="https://bootswatch.com/4/solar/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/scripts.js" type="text/javascript"></script>
</head>

<body>
    <main class="contenedor seccion contenido-centrado">
            <form name="FormDatos" class="contacto" action="guardar.php" method="post" onsubmit="return validateForm()">
            <div class="contenedor clearfix">
                    <fieldset>
                        <legend>Registro de Datos Conferencia</legend>

                        <label for="nombre">Nombres:</label>
                        <input type="text" id="nombre" name="nombre" placeholder="Ingrese Nombre" pattern="[A-Z a-z]+" title="Solo letras">
                        
                        <label for="apellido">Apellidos:</label>
                        <input type="text" id="apellido" name="apellido" placeholder="Ingrese Apellido" pattern="[A-Z a-z]+" title="Solo letras">
                        
                        <label for="telefono">Edad:</label>
                        <input type="number" id="telefono" name="telefono" min="10" max="50" placeholder="Ingrese Edad">

                        <label for="email">Email:</label>
                        <input type="email" id="email" name="email" placeholder="Ingresar Email">

                        <label for="mensaje">Tipo de Conferencia:</label>
                        <textarea id="mensaje" name="mensaje"></textarea>
                    </fieldset>
                    
                <input type="submit" name="registrodatos" value="Enviar" class="boton boton-azul">
            </div>
        </form>
    </main>

</body>
</html>